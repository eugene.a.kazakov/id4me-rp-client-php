<?php

namespace Id4me\RP;

use GuzzleHttp\Client as GuzzleClient;
use Id4me\RP\Model\Client;
use Id4me\RP\Model\OpenIdConfig;

/**
 * Class responsible of handling registration process
 */
class Registration
{
    /**
     * @var GuzzleClient;
     */
    private $httpClient;

    /**
     * Registration constructor.
     */
    public function __construct()
    {
        $this->httpClient = new GuzzleClient();
    }

    /**
     * Register specific client data using given identity authority
     *
     * @param OpenIdConfig $openIdConfig OpenId Config Instance containing data fetched from authority
     * @param string $identifier identifier of relying part request open id data (might be a domain or user name)
     * @param array|string $redirectUrls one or several urls of relying part requesting to be redirected to after successful user authentication
     * @param string $application_type application type triggering registration process by authority
     *
     * @return Client
     */
    public function register(
        OpenIdConfig $openIdConfig,
        string $identifier,
        $redirectUrls,
        string $application_type = 'web'
    ) {
        return $this->registerClient($openIdConfig, $identifier, $redirectUrls, $application_type);
    }

    /**
     * Set current http client to another value
     *
     * @param Client $httpClient
     */
    public function setHttpClient(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * Retrieves given client enriched with registration data
     *
     * @param OpenIdConfig $openIdConfig OpenId Config Instance containing data fetched from authority
     * @param string $identifier identifier of relying part request open id data (might be a domain or user name)
     * @param array|string $redirectUrls one or several urls of relying part requesting to be redirected to after successful user authentication
     * @param string $application_type application type triggering registration process by authority
     *
     * @return Client
     */
    private function registerClient(
        OpenIdConfig $openIdConfig,
        string $identifier,
        $redirectUrls,
        string $application_type = 'web'
    ) {
        $issuer               = $openIdConfig->getIssuer();
        $registrationEndPoint = $openIdConfig->getRegistrationEndpoint();

        $registrationRequest = [
            'client_name'      => $identifier,
            'application_type' => $application_type,
            'redirect_uris'    => is_array($redirectUrls) ? $redirectUrls : [$redirectUrls],
        ];

        $result = $this->httpClient->post($registrationEndPoint, [
            'headers' => ['content-type' => 'application/json'],
            'body'    => json_encode($registrationRequest)
        ]);

        $registrationData = json_decode($result->getBody()->getContents(), true);

        $activeRedirectUrl = is_array($redirectUrls) ? reset($redirectUrls) : $redirectUrls;

        return $this->createClient($registrationData, $issuer, $activeRedirectUrl);
    }

    /**
     * Retrieves properties identified by given property
     *
     * @param array $properties
     * @param string $property
     *
     * @return mixed|null
     */
    private function fetchClientProperty(array $properties, string $property)
    {
        return (isset($properties[$property])) ? $properties[$property] : null;
    }

    /**
     * Creates an instance of openId Client with given data
     *
     * @param array $registrationData
     * @param string $issuer
     * @param string $redirectUrl
     *
     * @return Client
     */
    private function createClient(array $registrationData, string $issuer, string $redirectUrl): Client
    {
        $client = new Client($issuer);

        $client->setClientName($this->fetchClientProperty($registrationData, 'client_name'));
        $client->setClientId($this->fetchClientProperty($registrationData, 'client_id'));
        $client->setClientSecret($this->fetchClientProperty($registrationData, 'client_secret'));
        $client->setClientExpirationTime((int)$this->fetchClientProperty($registrationData, 'client_secret_expires_at'));
        $client->setActiveRedirectUri($redirectUrl);
        $client->setRedirectUris($this->fetchClientProperty($registrationData, 'redirect_uris'));

        return $client;
    }
}
