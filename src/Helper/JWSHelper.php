<?php

namespace Id4me\RP\Helper;

use Firebase\JWT\JWT;
use Firebase\JWT\SignatureInvalidException;

use FG\ASN1\Universal\BitString;
use FG\ASN1\Universal\Integer;
use FG\ASN1\Universal\NullObject;
use FG\ASN1\Universal\ObjectIdentifier;
use FG\ASN1\Universal\Sequence;

use Base64Url\Base64Url;

/**
 * This class is responsible of handling RSA encryption and decryption functions
 * used for example for validation of authority data fetched by Relying Party
 */
class JWSHelper
{
    /**
     * Retrieves a JWS out of given jws json string
     *
     * @param string $jws
     *
     * @return array
     *
     * @throws \Exception
     */
    private function createJWSFromString(string $jws)
    {
        $parts = \explode('.', $jws);
        if (3 !== \count($parts)) {
            throw new \InvalidArgumentException('Unsupported input');
        }

        $encodedProtectedHeader = $parts[0];
        $protectedHeader = \json_decode(Base64Url::decode($encodedProtectedHeader), true);
        if (empty($parts[1])) {
            $payload = null;
            $encodedPayload = null;
            $isPayloadDetached = true;
        } else {
            $encodedPayload = $parts[1];
            $isPayloadEncoded = !\array_key_exists('b64', $protectedHeader) || true === $protectedHeader['b64'];
            $payload = $isPayloadEncoded ? Base64Url::decode($encodedPayload) : $encodedPayload;
            $isPayloadDetached = false;
        }
        $signature = Base64Url::decode($parts[2]);

        return [$payload, $signature, $isPayloadDetached, $encodedProtectedHeader];
    }

    /**
     * Verifies given jws token string with given jws data set string ()
     *
     * @param string $jwsTokenString
     * @param string $jwsSetJson
     *
     * @return bool
     *
     * @throws \Exception
     */
    public function verifySignature(string $jwsTokenString, $jwsSetJson)
    {
        $jwkSet = $this->createJWKSet($jwsSetJson);
        try {
            JWT::decode($jwsTokenString, $jwkSet, ['RS256']);

            return true;
        } catch (SignatureInvalidException $e) {
            return false;
        }
    }

    private function createJWKSet($jwsSetJson): array
    {
        $data = \json_decode($jwsSetJson, true);

        if (!\is_array($data) || !\array_key_exists('keys', $data) || !\is_array($data['keys'])) {
            throw new \InvalidArgumentException('Invalid data.');
        }

        $keys = [];
        foreach ($data['keys'] as $key) {
            if (!\array_key_exists('kty', $key)) {
                throw new \InvalidArgumentException('The parameter "kty" is mandatory.');
            }

            if (\array_key_exists('kid', $key)) {
                if ($key['kty'] === 'RSA') {
                    $keys[$key['kid']] = $this->createRSAPublicKey($key);
                }

                continue;
            }
        }
        return $keys;
    }

    private function createRSAPublicKey(array $key)
    {
        $sequence = new Sequence();
        $oid_sequence = new Sequence();
        $oid_sequence->addChild(new ObjectIdentifier('1.2.840.113549.1.1.1'));
        $oid_sequence->addChild(new NullObject());
        $sequence->addChild($oid_sequence);

        $n = new Integer($this->fromBase64ToInteger($key['n']));
        $e = new Integer($this->fromBase64ToInteger($key['e']));
        $key_sequence = new Sequence();
        $key_sequence->addChild($n);
        $key_sequence->addChild($e);
        $key_bit_string = new BitString(\bin2hex($key_sequence->getBinary()));
        $sequence->addChild($key_bit_string);

        $result = "-----BEGIN PUBLIC KEY-----\n";
        $result .= \chunk_split(\base64_encode($sequence->getBinary()), 64, "\n");
        $result .= "-----END PUBLIC KEY-----\n";

        $publicKey = \openssl_pkey_get_public($result);
        if (false === $publicKey) {
            throw new \InvalidArgumentException('Invalid public key: ' . \openssl_error_string());
        }
        return $publicKey;
    }

    private function fromBase64ToInteger(string $value): string
    {
        return $this->baseConvert(\current(\unpack('H*', Base64Url::decode($value))), 16, 10);
    }

    private function baseConvert(string $number, int $fromBase, int $toBase): string
    {
        $digits = '0123456789abcdefghijklmnopqrstuvwxyz';
        $length = \strlen($number);
        $result = '';

        $nibbles = [];
        for ($i = 0; $i < $length; ++$i) {
            $nibbles[$i] = \strpos($digits, $number[$i]);
        }

        do {
            $value = 0;
            $newlen = 0;
            for ($i = 0; $i < $length; ++$i) {
                $value = $value * $fromBase + $nibbles[$i];
                if ($value >= $toBase) {
                    $nibbles[$newlen++] = (int)($value / $toBase);
                    $value %= $toBase;
                } elseif ($newlen > 0) {
                    $nibbles[$newlen++] = 0;
                }
            }
            $length = $newlen;
            $result = $digits[$value] . $result;
        } while ($newlen !== 0);

        return $result;
    }

    /**
     * Retrieves JWS payload of decrypted value of given jws token string
     *
     * @param string $jwsTokenString
     *
     * @return string
     *
     * @throws \Exception
     */
    public function getJWSPayload(string $jwsTokenString)
    {
        list($payload) = $this->createJWSFromString($jwsTokenString);

        return $payload;
    }
}
